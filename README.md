# NEAR Setup Guide for Mainnet Validators

### NEAR is a new generation sharded proof-of-stake public Blockchain with extensive smart contract layer. Developers can learn more about NEAR Blockchain here: https://near.org/

### This is practical guide for NEAR Validator candidates on setting up validator node utilizing Docker container built by CryptoTribe.

#### Disclaimer: Prior to setting up Mainnet node, you need to run validator on Testnet first. Official documentation here: https://docs.near.org/docs/validator/staking

#### Step #1: Create NEAR account at https://wallet.near.org/ 

You will need some NEAR tokens to fund account creation. You could buy some on Binance and transfer to target address during account creation.

#### Step #2: Setup your server with NEAR software. Recommended baremetal server with at least 4 CPU cores, 16GB RAM and 100GB SSD drive.

2.1 Download NEAR client with npm: `npm i -g near-cli`

#### Step #3: Create and deploy your Staking pool contract using near-cli:

3.1 $ `export NODE_ENV=mainnet`

3.2 $ `near login` (Follow link in browser to authenticate with your NEAR account created in step #1)

3.3 Create Staking Pool:

$ `near call poolv1.near create_staking_pool '{"staking_pool_id":"<POOL_ID>", "owner_id":"<ACCOUNT_ID>", "stake_public_key":"<VALIDATOR_PUB_KEY>", "reward_fee_fraction": {"numerator": <PERCENT>, "denominator": 100}}' - account_id <ACCOUNT_ID>  - amount 30  - gas 300000000000000`

```
Where: 
POOL_ID - Validator pool name, ex. 'nearrocks'
ACCOUNT_ID - NEAR account id created in step #1
VALIDATOR_PUB_KEY - Validator public key
PERCENT - Validator rewards share claimed for operational costs, ex. 10.
```


3.4 Deposit tokens to Staking pool:

$ `near call <POOL_ID>.poolv1.near deposit '{}' - accountId <ACCOUNT_ID> - amount <NEAR_TOKENS>` 

3.5 Stake deposited # of Near (in #*10^24 YoctoNEAR) tokens in staking pool:

$ `near call <POOL_ID>.poolv1.near stake '{"amount": "#000000000000000000000000"}' - accountId <ACCOUNT_ID>`

3.6 Get staked balance in pool:

$ `near view <POOL_ID>.poolv1.near get_account_staked_balance '{"account_id": "<ACCOUNT_ID>"}'`


#### Step #4: Run NEAR validator node using Docker container: https://gitlab.com/cryptotribe/near-docker

Put genesis.json, node_key.json, config.json, validator_key.json files in the ./near folder for the appropriate network.

More details on mainnet configuration: https://docs.near.org/docs/validator/deploy-on-mainnet 

4.1 Start mainnet node for syncing:

$ `docker run -v /root/.near -p 3032:3032 -p 24569:24569 -d --name near_mainnet eostribe/nearup-mainnet run mainnet --account-id <POOL_ID>.poolv1.near`

4.2 Ping pool contract to get it listed:

$ `near call <POOL_ID>.poolv1.near  ping '{}' --accountId <ACCOUNT_ID>` 

4.3 Confirm pool shows up in online nodes: https://explorer.near.org/nodes/online-nodes

Sufficient stake necessary to take a validator seat: https://docs.near.org/docs/validator/economics 

#### Step #5: Unstaking and withdrawal of validator rewards portion:

5.1 Unstake # of NEAR tokens (in #*10^24 YoctoNEAR) in pool contract:

$ `near call <POOL_ID>.poolv1.near unstake '{"amount": "#000000000000000000000000"}' --accountId <ACCOUNT_ID>`

5.2 View staked/unstaked balance state on account:

$ `near view <POOL_ID>.poolv1.near get_account '{"account_id": "<ACCOUNT_ID>"}'`

5.3 Withdraw # of NEAR tokens (in #*10^24 YoctoNEAR) after 36hrs unstaking period:

$ `near call <POOL_ID>.poolv1.near withdraw '{"amount": "#000000000000000000000000"}' --accountId=<ACCOUNT_ID> --gas=100000000000000`

### Reference links

https://docs.near.org/docs/validator/staking 

https://docs.near.org/docs/validator/deploy-on-mainnet

https://explorer.near.org/nodes/validators 

https://near.org/technology/ 
